import sublime
import sublime_plugin
import bisect
import time
import html

KEY_ID = "symballoon"


class ShowSymbolBalloonCommand(sublime_plugin.TextCommand):

	timer = 0

	def run(self, view):

		def navigate(href):
			vw.show(int(href), show_surrounds=False,
									animate=True,
									keep_to_left=True)
			return


		def annotation_navigate(href):
			vw.erase_regions(KEY_ID)
			return


		def get_indent(point):
			return vw.extract_scope(point).b - point


		if time.time() - self.timer < 0.33:
			self.timer = time.time()
			return
		
		self.timer = time.time()

		vw = self.view
		vrgn = vw.visible_region()
		vpoint = vrgn.a
		endpt = vrgn.b
		rgns = vw.symbol_regions()
		symbol_pts = [rgn.region.a  for rgn in rgns]
		index = bisect.bisect_left(symbol_pts, vpoint)

		if index <= 0:
			return

		issource = "Markdown" not in vw.syntax().name
		
		if issource:
			indentlvl = vw.indentation_level
			
			while vw.classify(vpoint) & sublime.CLASS_EMPTY_LINE:
				vpoint += 1
				if vpoint >= endpt:
					return

			indents = [indentlvl(pt)  for pt in symbol_pts[:index]]
			tgt_idt = min(indents[-1], indentlvl(vpoint) - 1)

		else:
			indents = [get_indent(pt)  for pt in symbol_pts[:index]]
			tgt_idt = indents[-1]

		symbol_dic = dict(zip(indents, symbol_pts))
		
		for idt in symbol_dic.copy():
			if idt > tgt_idt:
				del symbol_dic[idt]
		
		if len(symbol_dic) == 0:
			return

		errpts = []
		tabsize = int(vw.settings().get('tab_size', 4))
		pkg_settings = sublime.load_settings("SymbolBalloon.sublime-settings")
		
		if issource:

			ignorchr = pkg_settings.get("ignored_characters", "")

			region = sublime.Region(max(symbol_dic.values()), vpoint)
			linestart_pts = [rgn.a  for rgn in vw.lines(region)
												if not rgn.empty()]
			indent_set = set(symbol_dic)
			closed_set = set()
			for pt in linestart_pts[2:3000]:
				idt = indentlvl(pt)
				if idt in closed_set:
					continue

				idtchr = vw.substr(pt)
				idt_width = (idt * tabsize) if idtchr == " " else idt
				idtend = pt + idt_width
				if vw.classify(idtend) & sublime.CLASS_LINE_END:
					errpts.append(pt)

				elif vw.substr(idtend) not in ignorchr:
					closed_set.add(idt)
					if indent_set <= closed_set:
						break

			if len(closed_set) > 0:
				for idt in symbol_dic.copy():
					if idt >= min(closed_set):
						del symbol_dic[idt]


		ahref = ""
		for pt in sorted(symbol_dic.values()):
			row = vw.rowcol(pt)[0] + 1
			symbol = html.escape(vw.substr(vw.line(pt)))
			ahref += ('<a class="noline" '
					'href="{0}">{1}..{2}</a><br>').format(pt, symbol, row)

		ahref = ahref.replace(' ' * tabsize, '\t')
		ahref = ahref.replace('\t', '&nbsp;' * tabsize)
		con = ('<body>' + _stylesheet() + '<div class="arrow"></div>'
				'<div class="balloon">' + ahref + '</div></body>')

		vw.erase_phantoms(KEY_ID)
		vw.erase_regions(KEY_ID)
		vw.hide_popup()	

		if bool(pkg_settings.get("popup_mode")):
			vw.show_popup(con, 
							max_width=800,
							location=vpoint,
							on_hide=True,
							on_navigate=navigate)
		else:
			vw.add_phantom(KEY_ID,
							sublime.Region(vpoint),
							con,
							sublime.LAYOUT_BELOW,
							on_navigate=navigate)
		
		if len(errpts) == 0  or not bool(
								pkg_settings.get("show_indent_error")):
			return

		vw.add_regions(KEY_ID,
						[sublime.Region(pt)  for pt in errpts],
						annotations=[_annotation_html()] * len(errpts),
						annotation_color="#aa0",
						on_navigate=annotation_navigate)



def _annotation_html():
	return ('<body><a style="text-decoration: none" href="">x</a>'
			'　indent err</body>')


def _stylesheet():
	return '''
		<style>
			.arrow {
				border-top: 0.3rem solid transparent;
				border-left: 0.5rem solid color(#dcf a(0.25));
				width: 0;
				height: 0;
			}
			.noline{
				text-decoration: none;
				font-size: 0.95rem;
				color: var(--foreground)
			}
			.balloon {
				display: block;
				text-decoration: none;
				background-color: color(#dcf a(0.25));
				padding: 0.1rem 1.2rem 0.1rem 0.4rem;
				border-radius: 0 0.7rem 0.7rem 0.6rem;
			}
		</style>
	'''


class EraseSymbolBalloonCommand(sublime_plugin.TextCommand):

	def run(self, view):
		self.view.erase_phantoms(KEY_ID)
		if len(self.view.get_regions(KEY_ID)) != 0 :
			self.view.erase_regions(KEY_ID)
		
