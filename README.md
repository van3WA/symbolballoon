## SymbolBalloon

画面上部の行が属するシンボルを表示します。
ST4085以降。

### Key Bindings

```
[
	{ "keys": ["ctrl+up"], "command": "run_macro_file", "args": {"file": "res://Packages/SymbolBalloon/Show Balloon.sublime-macro"} },
	{ "keys": ["ctrl+down"], "command": "run_macro_file", "args": {"file": "res://Packages/SymbolBalloon/Erase Balloon.sublime-macro"} },
]
```

- ctrl+up:　Show Balloon
- ctrl+down:　Erase Balloon

同梱のマクロを呼び出します。


#### 単独動作の場合

```
[
	{ "keys": ["  "], "command": "show_symbol_balloon" },
	{ "keys": ["  "], "command": "erase_symbol_balloon" },
]
```

コマンドパレットからでも実行可能。  
ctrl+shift+p  
"symbolballoon"  


### Settings

"popup_mode": false

"show_indent_error": false

"ignored_characters": "{[<()\"'/*#"  
シンボルと同一インデントにあっても閉じたとみなさないキャラクタ。
シンボルの次の行は解析しません。
